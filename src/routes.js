import  { BrowserRouter, Route } from "react-router-dom";
import React from "react";
import AboutGame from './components/aboutGame';
import AboutEmmanuel from './components/aboutEmmanuel';
import DummyA from './components/dummyA';
import DummyB from './components/dummyB';
import Game from './components/game';

const Router = () => {
    return(
        <BrowserRouter>
          <Route exact path="/">
            <Game />
          </Route>
          <Route exact path="/about-game">
            <AboutGame />
          </Route>
          <Route exact path="/about-me">
            <AboutEmmanuel />
          </Route>
          <Route exact path="/dummy-a">
            <DummyA />
          </Route>
          <Route exact path="/dummy-b">
            <DummyB />
          </Route>
      </BrowserRouter>
    )
}

export default Router;