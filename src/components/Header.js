import React from "react";
import '../css/main.css';
import { Link } from "react-router-dom";

const App = ({children}) =>  {
  return (
    <center>
      <div className="header">
        <Link className="header-item" to='/'>Tic Tac Toe</Link>
        <Link className="header-item" to='/about-game'>About Tic Tac Toe</Link>
        <Link className="header-item" to='/about-me'>About Emmanuel</Link>
        <Link className="header-item" to='/dummy-a'>Dummy 1</Link>
        <Link className="header-item" to='/dummy-b'>Dummy 2</Link>
      </div>
      <center>
        {children}
      </center>
    </center>

  );
}

export default App;
