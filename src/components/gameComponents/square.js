import React from "react";
import '../../css/main.css';

const Square = ({value, onClickFunc}) => {
    return(
        <div className="box" onClick={onClickFunc}>{value}</div>
    );
}

export default Square;