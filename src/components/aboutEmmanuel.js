import React from "react";
import Header from './Header';
import '../css/main.css';

const AboutMe = () => {
    return(
        <Header>
            <div className="container">
                <p>
                    My linkedin profile says: <span className="quote">I am 26 years old, I have more than 5 years of experience as a web developer, currently specialized in front development. More than 2 years working in the fintech industry in Chile. Expert in the entire React and React Native technology stack, with backend experience with PHP, Ruby and Node. I am a fan of working on new projects and meeting new opportunities.</span>
                </p>
                <iframe src="https://giphy.com/embed/ekjmhJUGHJm7FC4Juo" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>
            </div>
        </Header>
    );
}

export default AboutMe;