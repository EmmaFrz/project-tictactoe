import React from "react";
import Header from './Header';
import '../css/main.css';

const AboutGame = () => {
    return(
        <Header>
            <div className="container">
                <p>
                    Wikipedia says: <span className="quote">"Tic-tac-toe, noughts and crosses, or Xs and Os is a paper-and-pencil game for two players who take turns marking the spaces in a three-by-three grid with X or O. The player who succeeds in placing three of their marks in a horizontal, vertical, or diagonal row is the winner."</span>
                </p>
                <iframe src="https://giphy.com/embed/YnZPEeeC7q6pQEZw1I" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>
            </div>
        </Header>
    );
}

export default AboutGame;