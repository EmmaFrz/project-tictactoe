import React, { useEffect, useState } from "react";
import Header from './Header';
import Square from "./gameComponents/square";
import {Wins} from './gameComponents/wins';
import '../css/main.css';

const DummyA = () => {
    const [values, setValues] = useState(['','','','','','','','','']);
    const [pointer, setPointer] = useState('X');
    const [gameResult, setGameResult] = useState({winner:'', type:'win'})
    const [winsCount, setWinsCounts] = useState([])


    const clickSquare = (index) => {
        let content = [...values];
        let mainValue = content[index];
        if(mainValue === ''){
            mainValue = pointer;
            setPointer( prevState => prevState === 'X' ? 'O' : 'X' )
        }
        content[index] = mainValue;
        setValues(content);
    }

    const reset = () => {
        setValues(['','','','','','','','','']);
        setPointer('X')
        setGameResult({winner:'', type:'win'})
    }

    const tie = () => {
        let isBoardFull = true;
        values.forEach((sq) => {
          if (sq === "") {
            isBoardFull = false;
          }
        });
    
        if (isBoardFull) {
            setGameResult({ winner: "No One", type: "tie" });
            setWinsCounts(prevState => {
                return(
                    [...prevState, 'NC']
                )
            })
        }
    }

    const winner = () => {
        Wins.forEach(inx => {
            const initialValidator = values[inx[0]];
            if (initialValidator === '') return;
            let isWinner = true;

            inx.forEach(element => {
                if(values[element] !== initialValidator){
                    isWinner = false;
                }
            });

            if(isWinner && gameResult.winner === ''){
                setGameResult({winner: pointer === 'X' ? 'O' : 'X', type:'win'})
                setWinsCounts(prevState => {
                    return(
                        [...prevState, pointer === 'X' ? 'O' : 'X']
                    )
                })
            }
        })
    }

    useEffect(() => {
        winner();
        tie();
    }, [values]);

    useEffect(() => {
        if(gameResult.winner !== '' ){
            alert(`The Result was a ${gameResult.type} and the winner was ${gameResult.winner}`);
            reset();
        }
    }, [gameResult])

    const totalWinners = () => {
        let XWins = winsCount.filter( d => d === 'X');
        let OWins = winsCount.filter( d => d === 'O');
        let NCWins = winsCount.filter( d => d === 'NC');

        return(
            <>
                {`X total Wins ${XWins.length}`} 
                <br/>
                {`O total Wins ${OWins.length}`}
                <br />
                {`No contest total Wins ${NCWins.length}`}
                <h5>Last winners</h5>
                {winsCount.map((data, inx) => {
                    return(
                        <p key={inx}>{data} was the winner</p>
                    )
                })}  
            </>
        )
    }

    return(
        <Header>
            <div className="container">
                <div className="board">
                    {values.map((data, index) => {
                        return(
                            <Square key={Math.random()} value={data} onClickFunc={() => clickSquare(index)}/>
                        )
                    })}
                </div>
                <br />
                <button onClick={reset}>Reset</button>
                <br />
                <h4>Total Wins</h4>
                {totalWinners()}
            </div>
        </Header>
    );
}

export default DummyA;